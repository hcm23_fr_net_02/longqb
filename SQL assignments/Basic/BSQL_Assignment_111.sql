create database EMS

use EMS;
go

-- Create table Employee, Status = 1: are working
CREATE TABLE [dbo].[Employee](
	[EmpNo] [int] NOT NULL
,	[EmpName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[BirthDay] [datetime] NOT NULL
,	[Email] [nchar](30) not null
,	[DeptNo] [int] NOT NULL
, 	[MgrNo] [int]
,	[StartDate] [datetime] NOT NULL
,	[Salary] [money] NOT NULL
,	[Status] [int] NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
,	[Level] [int] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE Employee 
ADD CONSTRAINT PK_Emp PRIMARY KEY (EmpNo)
GO

ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Level] 
	CHECK  (([Level]=(7) OR [Level]=(6) OR [Level]=(5) OR [Level]=(4) OR [Level]=(3) OR [Level]=(2) OR [Level]=(1)))
GO
ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Status] 
	CHECK  (([Status]=(2) OR [Status]=(1) OR [Status]=(0)))

GO

ALTER TABLE [dbo].[Employee]
ADD CONSTRAINT chk_Email CHECK (Email IS NOT NULL)
GO

ALTER TABLE [dbo].[Employee] 
ADD CONSTRAINT chk_Email1 UNIQUE(Email)

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_EmpNo DEFAULT 0 FOR EmpNo

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_Status DEFAULT 0 FOR Status

GO
CREATE TABLE [dbo].[Skill](
	[SkillNo] [int] IDENTITY(1,1) NOT NULL
,	[SkillName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Skill
ADD CONSTRAINT PK_Skill PRIMARY KEY (SkillNo)

GO
CREATE TABLE [dbo].[Department](
	[DeptNo] [int] IDENTITY(1,1) NOT NULL
,	[DeptName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Department
ADD CONSTRAINT PK_Dept PRIMARY KEY (DeptNo)

GO
CREATE TABLE [dbo].[Emp_Skill](
	[SkillNo] [int] NOT NULL
,	[EmpNo] [int] NOT NULL
,	[SkillLevel] [int] NOT NULL
,	[RegDate] [datetime] NOT NULL
,	[Description] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT PK_Emp_Skill PRIMARY KEY (SkillNo, EmpNo)
GO

ALTER TABLE Employee  
ADD  CONSTRAINT [FK_1] FOREIGN KEY([DeptNo])
REFERENCES Department (DeptNo)

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_2] FOREIGN KEY ([EmpNo])
REFERENCES Employee([EmpNo])

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_3] FOREIGN KEY ([SkillNo])
REFERENCES Skill([SkillNo])

GO

-- Question 1
insert into Department(DeptName, Note) values 
	('A department', ''),
	('B department', ''),
	('C department', ''),
	('D department', ''),
	('E department', ''),
	('F department', ''),
	('G department', ''),
	('H department', '')

insert into Employee(EmpNo, EmpName, BirthDay, Email, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level) values 
	(1, 'Nguyen Van A', '2000-10-11', 'abc@gmail.com', 1, 2, '2019-2-7', 15000000, 1, '', 2),
	(2, 'Nguyen Van B', '2001-9-11', 'abd@gmail.com', 3, 1, '2018-9-7', 17000000, 1, '', 2),
	(3, 'Nguyen Van C', '2000-8-21', 'abe@gmail.com', 7, 1, '2018-9-7', 8000000, 1, '', 2),
	(4, 'Nguyen Van D', '2002-1-11', 'a@gmail.com', 5, 3, '2018-9-7', 4000000, 1, '', 2),
	(5, 'Nguyen Van E', '2000-1-11', 'bd@gmail.com', 2, 1, '2018-9-7', 5000000, 1, '', 2),
	(6, 'Nguyen Van F', '2000-1-11', 'ad@gmail.com', 8, 5, '2018-9-7', 6000000, 1, '', 2),
	(7, 'Nguyen Van G', '2000-1-11', 'ab@gmail.com', 4, 1, '2018-9-7', 1000000, 1, '', 2),
	(8, 'Nguyen Van H', '2000-1-11', 'absd@gmail.com', 6, 2, '2018-9-7', 3000000, 1, '', 2),
	(9, 'Nguyen Van I', '2000-1-11', 'abdt@gmail.com', 1, 2, '2018-9-7', 3000000, 1, '', 2),
	(10, 'Nguyen Van J', '2003-2-8', 'ads@gmail.com', 3, 4, '2023-7-27', 1000000, 2, '', 1)

insert into Skill(SkillName, Note) values 
	('Java', ''),
	('.NET', ''),
	('Golang', ''),
	('Python', ''),
	('PHP', ''),
	('Ruby', ''),
	('Julia', ''),
	('Perl', ''),
	('C++', '')

insert into Emp_Skill(SkillNo, EmpNo, SkillLevel, RegDate, [Description]) values 
	(1, 1, 7, '2018-7-17', ''),
	(1, 3, 4, '2017-2-16', ''),
	(2, 1, 2, '2018-1-1', ''),
	(3, 1, 5, '2019-5-7', ''),
	(1, 7, 1, '2023-7-17', ''),
	(4, 2, 3, '2018-7-17', ''),
	(5, 3, 3, '2018-7-17', ''),
	(6, 8, 7, '2016-7-17', ''),
	(7, 9, 6, '2018-7-17', ''),
	(8, 5, 2, '2021-7-17', ''),
	(6, 6, 4, '2020-7-17', ''),
	(5, 4, 6, '2018-7-17', ''),
	(2, 9, 3, '2022-7-17', ''),
	(1, 10, 1, '2023-7-30', ''),
	(9, 3, 2, '2021-9-10', '')

-- Question 2
select e.[EmpName], e.Email, d.DeptName
from Employee e join Department d on e.DeptNo = d.DeptNo
where DATEDIFF(m, [StartDate], GETDATE()) >= 6

-- Question 3
select e.EmpName
from Employee e join Emp_Skill es on e.EmpNo = es.EmpNo
	join Skill s on es.SkillNo = s.SkillNo
where s.SkillName = 'C++' or s.SkillName = '.NET'

-- Question 4
select e.EmpName as [Employee name], m.EmpName as [Manager name], m.Email as [Manager email]
from Employee e join Employee m on e.MgrNo = m.EmpNo

-- Question 5
select d.DeptNo, e.EmpNo, e.EmpName
from Department d join Employee e on d.DeptNo = e.DeptNo
where d.DeptNo in (select d1.DeptNo
						from Department d1 join Employee e1 on d1.DeptNo = e1.DeptNo
						group by d1.DeptNo
						having count(*) >= 2)

-- Question 6
select e.EmpName, e.Email, count(es.SkillNo) as numberOfSkills
from Employee e join Emp_Skill es on e.EmpNo = es.EmpNo
group by e.EmpName, e.Email
order by e.EmpName asc

-- Question 7
select e.*
from Employee e 
where e.EmpNo in (select es.EmpNo
					from Emp_Skill es 
					group by es.EmpNo 
					having count(*) > 1)
go
-- Question 8
create view WorkingEmployees as
select e.EmpName, s.SkillName, d.DeptName
from Employee e join Department d on e.DeptNo = d.DeptNo
	join Emp_Skill es on e.EmpNo = es.EmpNo
	join Skill s on s.SkillNo = es.SkillNo

go
select * from WorkingEmployees