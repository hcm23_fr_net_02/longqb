create database ASQL302

use ASQL302;

-- Question 1
/* Create the tables (with the most appropriate field/column constraints & types) 
	and add at least 3 records into each created table */

-- Part 1
-- Create table
create table San_Pham (
	Ma_SP int,
	Ten_SP nvarchar(100),
	Don_Gia int,
	primary key (Ma_SP)
)

create table Khach_Hang (
	Ma_KH int,
	Ten_KH nvarchar(100),
	Phone_No varchar(10),
	Ghi_Chu nvarchar(100),
	primary key (Ma_KH)
)

create table Don_Hang(
	Ma_DH int,
	Ngay_DH datetime,
	Ma_SP int,
	So_Luong int,
	Ma_KH int,
	primary key (Ma_DH, Ma_SP, Ma_KH),
	foreign key (Ma_SP) references San_Pham(Ma_SP),
	foreign key (Ma_KH) references Khach_Hang(Ma_KH)
)

-- insert at least 3 values to each table
insert into San_Pham(Ma_SP, Ten_SP, Don_Gia) values (1, 'Chocolate', 50000),
	(2, 'Butter', 70000),
	(3, 'Vanilla', 60000);

insert into Khach_Hang(Ma_KH, Ten_KH, Phone_No, Ghi_Chu) values (1, 'Khanh', 0909090909, 'Love butter'),
	(2, 'Quynh', 0707070707, 'Love vanilla'),
	(3, 'Uyen', 0202020202, 'Love chocolate');

insert into Don_Hang(Ma_DH, Ngay_DH, Ma_SP, So_Luong, Ma_KH) values (1, '2020-10-10', 1, 10, 3),
	(2, '2020-10-11', 2, 6, 1),
	(3, '2020-10-12', 3, 10, 2)

go

-- Question 2
/* Create an order slip VIEW which has the same number of lines 
	as the Don_Hang, with the following information: 
	Ten_KH, Ngay_DH, Ten_SP, So_Luong, Thanh_Tien */

create view [Oder slip] as 
select kh.Ten_KH, dh.Ngay_DH, sp.Ten_SP, dh.So_Luong, dh.So_Luong * sp.Don_Gia as Thanh_Tien
	from Don_Hang dh join San_Pham sp on dh.Ma_SP = sp.Ma_SP
		join Khach_Hang kh on dh.Ma_KH = kh.Ma_KH
go

-- test Question 1

select * from San_Pham
select * from Khach_Hang
select * from Don_Hang
select * from [Oder slip]

-- Question 2
-- Part 1
create table Department(
	Department_Number int,
	Department_Name nvarchar(50),
	primary key (Department_Number)
)

create table Employee_Table(
	Employee_Number int,
	Employee_Name nvarchar(100),
	Department_Number int,
	primary key (Employee_Number, Department_Number),
	foreign key (Department_Number) references Department(Department_Number)
)

create table Skill_Table(
	Skill_Code int,
	Skill_Name nvarchar(50),
	primary key (Skill_Code)
)

create table Employee_Skill_Table(
	Employee_Number int,
	Skill_Code int,
	Department_Number int,
	Date_Registered datetime,
	primary key (Employee_Number, Skill_Code),
	foreign key (Employee_Number, Department_Number) references Employee_Table(Employee_Number, Department_Number),
	foreign key (Skill_Code) references Skill_Table(Skill_Code)
)

--alter table Employee_Table add constraint FK_Department_Employee foreign key
--	(Department_Number) references Department_Table(Department_Number)

--alter table Employee_Skill_Table add constraint FK_Skill_Employee foreign key
--	(Skill_Code) references Skill_Table(Skill_Code)

--alter table Employee_Skill_Table add constraint FK_EmployeeNumber foreign key 
--	(Employee_Number) references Employee_Table(Employee_Number)

insert into Department(Department_Number, Department_Name) values (1, 'ABC Department'),
	(2, 'ACD Department'),
	(3, 'BER Department')

insert into Employee_Table(Employee_Number, Employee_Name, Department_Number) values 
	(1, 'Nguyen Van A', 1), 
	(2, 'Nguyen Van B', 1),
	(3, 'Nguyen Van C', 2),
	(4, 'Nguyen Van D', 3),
	(5, 'Nguyen Van E', 2),
	(6, 'Nguyen Van F', 2),
	(7, 'Nguyen Van G', 2)

insert into Skill_Table(Skill_Code, Skill_Name) values (1, 'Java'),
	(2, '.NET'),
	(3, 'Golang')

insert into Employee_Skill_Table(Employee_Number, Skill_Code, Date_Registered) values 
	(1, 1, '2020-8-7'),
	(2, 1, '2020-7-4'),
	(3, 3, '2020-11-10'),
	(4, 2, '2020-9-1'),
	(5, 1, '2021-11-9'),
	(6, 3, '2020-4-3'),
	(7, 2, '2019-1-1'),
	(3, 1, '2019-3-9'),
	(5, 2, '2022-10-11'),
	(1, 2, '2018-7-3'),
	(2, 3, '2021-7-8'),
	(4, 1, '2022-7-1')

-- Part 2
-- Use JOIN selection
select e.Employee_Name
from Employee_Table e join Employee_Skill_Table es on e.Employee_Number = es.Employee_Number
	join Skill_Table s on es.Skill_Code = s.Skill_Code
where s.Skill_Name = 'Java'

-- Use subquery
select e.Employee_Name
from Employee_Table e
where e.Employee_Number in (select es.Employee_Number
						from Employee_Skill_Table es
						where es.Skill_Code = (select s.Skill_Code from Skill_Table s where s.Skill_Name = 'Java'))

-- Part 3
select d.Department_Number, e.Employee_Number, e.Employee_Name
from Department d join Employee_Table e on d.Department_Number = e.Department_Number
where d.Department_Number in (select d1.Department_Number
						from Department d1 join Employee_Table e1 on d1.Department_Number = e1.Department_Number
						group by d1.Department_Number
						having count(*) >= 3)

-- Part 4
select e.*
from Employee_Table e 
where e.Employee_Number in (select es.Employee_Number 
					from Employee_Skill_Table es 
					group by es.Employee_Number 
					having count(*) > 1)
go
-- Part 5
create view Employees_With_Multiple_Skills as
select e.Employee_Number, e.Employee_Name, d.Department_Name
from Employee_Table e join Department d on e.Department_Number = d.Department_Number
where e.Employee_Number in (select es.Employee_Number 
					from Employee_Skill_Table es 
					group by es.Employee_Number 
					having count(*) > 1)
go

select * from Employees_With_Multiple_Skills
drop view Employees_With_Multiple_Skills