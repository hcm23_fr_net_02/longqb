﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Text.Json;
using System.Text.Json.Nodes;
using Newtonsoft.Json;
//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;
using System.Threading.Tasks;
using System.Net.Http.Json;

namespace ToDoListWithHttp
{
    public class TodoListManager
    {
        private HttpClient _httpClient;
        private string _url;
        public TodoListManager(HttpClient httpClient)
        {
            _httpClient = httpClient;
            _url = "https://651e69de44a3a8aa47684bf6.mockapi.io/Items";
        }

        public async Task DisplayAsync()
        {
            HttpResponseMessage responseMessage = await
                    _httpClient.GetAsync(_url);

            if (responseMessage.IsSuccessStatusCode)
            {
                string content = await responseMessage.Content.ReadAsStringAsync();
                List<TodoItem> todoList = JsonConvert.DeserializeObject<List<TodoItem>>(content);

                for (int i = 0; i < todoList?.Count; i++)
                {
                    Console.WriteLine($"{todoList[i].Id} - " +
                        $"{todoList[i].Description} - {todoList[i].Created} - " +
                        $"{todoList[i].Status}");
                }
            }
        }

        public async Task<bool> AddItemAsync(TodoItem item) 
        {
            //string sendingData = JsonConvert.SerializeObject(item);
            HttpResponseMessage responseMessage = await
                _httpClient.PostAsJsonAsync(_url, item);
            if (responseMessage.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }

        public async Task<TodoItem> GetItemAsync(int id)
        {
            HttpResponseMessage responseMessage = await 
                _httpClient.GetAsync($"{_url}/{id}");
            if (responseMessage.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<TodoItem>(
                    await responseMessage.Content.ReadAsStringAsync());
            }
            return null;
        }

        public async Task<bool> UpdateItemAsync(TodoItem item)
        {
            HttpResponseMessage responseMessage = await
                _httpClient.PutAsJsonAsync($"{_url}/{item.Id}", item);

            if (responseMessage.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }


        public async Task<bool> DeleteItemAsync(int id)
        {
            HttpResponseMessage responseMessage = await
                _httpClient.DeleteAsync($"{_url}/{id}");
            if (responseMessage.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }
    }
}
