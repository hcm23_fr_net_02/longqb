﻿
using ToDoListWithHttp;

HttpClient httpClient = new HttpClient();
TodoListManager manager = new TodoListManager(httpClient);
int count = 1;

int option;

do
{
    Console.WriteLine("--------- TO DO LIST MENU ---------");
    Console.WriteLine("1. Display all to-do items");
    Console.WriteLine("2. Add new to-do item");
    Console.WriteLine("3. Remove a to-do item");
    Console.WriteLine("4. Mark a to-do  item as done");
    Console.WriteLine("5. Change the priority of a to-do item");
    Console.WriteLine("6. Exit");
    Console.Write("Choose: ");
    option = int.Parse(Console.ReadLine());

    switch(option)
    {
        case 1:
            await manager.DisplayAsync();
            break;
        case 2:
            TodoItem item = new TodoItem();
            Console.Write("Enter to-do job description: ");
            item.Description = Console.ReadLine();
            Console.WriteLine("Choose the priority of this job: ");
            Console.WriteLine("1. Low");
            Console.WriteLine("2. Medium");
            Console.WriteLine("3. High");
            Console.Write("Enter the priority code number: ");
            int input = int.Parse(Console.ReadLine());
            switch (input)
            {
                case 1:
                    item.Status = PriorityCode.Low;
                    break; 
                case 2:
                    item.Status = PriorityCode.Medium;
                    break;
                case 3:
                    item.Status = PriorityCode.High;
                    break;
                default:
                    break;
            }
            if (await manager.AddItemAsync(item))
            {
                Console.WriteLine("Add new to do successfully!");
            }
            else
            {
                Console.WriteLine("Fail to add new item");
            }
            break;
        case 3:
            Console.Write("Enter the to-do item id you want to delete: ");
            int id = int.Parse(Console.ReadLine());
            if (await manager.DeleteItemAsync(id))
            {
                Console.WriteLine("Delete item successfully");
            }
            else
            {
                Console.WriteLine("Fail to delete the item");
            }
            break;
        case 4:
            Console.Write("Enter the to-do item id you want to mark as done: ");
            id = int.Parse(Console.ReadLine());
            item = await manager.GetItemAsync(id);
            item.IsDone = true;
            if (await manager.UpdateItemAsync(item))
            {
                Console.WriteLine("Update successfully");
            }
            else
            {
                Console.WriteLine("Fail to update the item");
            }            
            break;
        case 5:
            Console.Write("Enter the to-do item id you want to update status: ");
            id = int.Parse(Console.ReadLine());
            item = await manager.GetItemAsync(id);
            Console.Write("Enter the priority code number: ");
            input = int.Parse(Console.ReadLine());
            switch (input)
            {
                case 1:
                    item.Status = PriorityCode.Low;
                    break;
                case 2:
                    item.Status = PriorityCode.Medium;
                    break;
                case 3:
                    item.Status = PriorityCode.High;
                    break;
                default:
                    break;
            }

            if (await manager.UpdateItemAsync(item))
            {
                Console.WriteLine("Update successfully");
            }
            else
            {
                Console.WriteLine("Fail to update the item");
            }
            break;
        default:
            option = 0;
            break;
    }
} while (option != 0);