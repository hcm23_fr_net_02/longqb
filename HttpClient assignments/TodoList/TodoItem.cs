﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoListWithHttp
{
    public enum PriorityCode
    {
        Low,
        Medium,
        High
    }
    public class TodoItem
    {
        private int _id;
        private string _description;
        private DateTime _created;
        private bool _isDone;
        private PriorityCode _status;

        public int Id { get { return _id; } }
        public string Description { 
            get { return _description; } 
            set { _description = value; } 
        }
        public DateTime Created { get { return _created; } }
        public PriorityCode Status { 
            get { return _status; } 
            set { _status = value; }
        }
        public bool IsDone { 
            get { return _isDone; } 
            set { _isDone = value; }
        }

        public TodoItem()
        {
            _created = DateTime.Now;
            _isDone = false;
            _description = string.Empty;
        }

        public TodoItem(int id, string description, DateTime created, PriorityCode status, bool isDone)
        {
            _id = id;
            _description = description;
            _created = created;
            _status = status;
            _isDone = isDone;
        }
    }
}
