﻿using System.Diagnostics;

var stopWatch = Stopwatch.StartNew(); 
stopWatch.Start();

List<string> photoUrl = new List<string>()
{
    "https://i.natgeofe.com/k/dfc7bec2-0657-4887-81a7-6d024a8c3f70/WH-XmasTree_2x1.jpg",
    "https://image.vtc.vn/resize/th/upload/2020/12/14/chuc-mung-giang-sinh-1200x800-22113811.jpg",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/NativityChristmasLights2.jpg/1200px-NativityChristmasLights2.jpg"
};

HttpClient httpClient = new HttpClient();

int count = 0;

for (int i = 0; i < photoUrl.Count; i++)
{
    var uri = new Uri(photoUrl[i]); 

    var uriWithoutQuery = uri.GetLeftPart(UriPartial.Path);
    var fileExtension = Path.GetExtension(uriWithoutQuery);

    var path = Path.Combine("PhotoDownloader//", $"Image{count++}{fileExtension}");
    Directory.CreateDirectory("PhotoDownloader");

    var imageBytes = await httpClient.GetByteArrayAsync(uri);
    await File.WriteAllBytesAsync(path, imageBytes);
}

stopWatch.Stop();
Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch = Stopwatch.StartNew();
stopWatch.Start();

var uri1 = new Uri(photoUrl[0]);
var uri2 = new Uri(photoUrl[1]);
var uri3 = new Uri(photoUrl[2]);

var uriWithoutQuery1 = uri1.GetLeftPart(UriPartial.Path);
var uriWithoutQuery2 = uri1.GetLeftPart(UriPartial.Path);
var uriWithoutQuery3 = uri1.GetLeftPart(UriPartial.Path);

var fileExtension1 = Path.GetExtension(uriWithoutQuery1);
var fileExtension2 = Path.GetExtension(uriWithoutQuery2);
var fileExtension3 = Path.GetExtension(uriWithoutQuery3);

var path1 = Path.Combine("PhotoDownloader//", $"Image{count++}{fileExtension1}");
var path2 = Path.Combine("PhotoDownloader//", $"Image{count++}{fileExtension2}");
var path3 = Path.Combine("PhotoDownloader//", $"Image{count++}{fileExtension3}");
Directory.CreateDirectory("PhotoDownloader");

var getByteImg1 = httpClient.GetByteArrayAsync(uri1);
var getByteImg2 = httpClient.GetByteArrayAsync(uri2);
var getByteImg3 = httpClient.GetByteArrayAsync(uri3);

var imageBytes1 = await getByteImg1;
var imageBytes2 = await getByteImg2;
var imageBytes3 = await getByteImg3;

var saveImg1 = File.WriteAllBytesAsync(path1, imageBytes1);
var saveImg2 = File.WriteAllBytesAsync(path2, imageBytes2);
var saveImg3 = File.WriteAllBytesAsync(path3, imageBytes3);

await saveImg1;
await saveImg2;
await saveImg3;

stopWatch.Stop();
Console.WriteLine(stopWatch.ElapsedMilliseconds);