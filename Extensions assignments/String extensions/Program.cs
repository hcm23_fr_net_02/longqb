﻿using System.Text;

string a = "Hello World!";
Console.WriteLine(a.DaoChuoi());
Console.WriteLine(a.RemoveDuplicates());

static class MyExtension
{
    public static string DaoChuoi(this string s)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = s.Length - 1; i >= 0; i--)
        {
            sb.Append(s[i]);
        }
        return sb.ToString();
    }

    public static string RemoveDuplicates(this string s)
    {
        HashSet<char> record = new HashSet<char>();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < s.Length; i++)
        {
            if (record.Add(s[i]))
            {
                sb.Append(s[i]);
            }
        }
        return sb.ToString();
    }
}

