﻿List<int> l = new List<int> { 1, 2, 3, 4, 7 };
Console.WriteLine(l.GetAverage());

static class MyExtension
{
    public static double GetAverage(this List<int> l)
    {
        double sum = 0d;
        foreach (int i in l)
        {
            sum += i;
        }
        return l.Count > 0 ? sum / l.Count: 0d;
    }
}