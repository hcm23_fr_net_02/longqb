using DemoStackAndQueue;

namespace UnitTestForStackAndQueueDemo
{
    public class Tests
    {
        //MyStack<int> intStack;
        //MyStack<string> stringStack;
        //MyStack<Person> personStack;
        //MyStack<Student> studentStack;

        //[SetUp]
        //public void Setup()
        //{
        //    intStack = new MyStack<int>(10);
        //    stringStack = new MyStack<string>(10);
        //    personStack = new MyStack<Person>(10);
        //    studentStack = new MyStack<Student>(10);
        //}

        [Test]
        public void Push_PushNewItemToIntStack_SuccessfullyAddNewItem()
        {
            MyStack<int> intStack = new MyStack<int>(100);
            int newItem = 5;
            bool expectedIsEmptyResult = false;

            intStack.Push(newItem);

            Assert.That(intStack.Peek(), Is.EqualTo(newItem));
            Assert.That(intStack.IsEmpty, Is.EqualTo(expectedIsEmptyResult));
        }

        [Test]
        public void Push_PushMoreItemsThanStackLimit_ThrowException()
        {
            MyStack<int> intStack = new MyStack<int>(1);

            Assert.Throws(Is.TypeOf<IndexOutOfRangeException>(), 
                   delegate { AddMoreItemThanStackLimit<int>(intStack, 1, 10); });
        }

        void AddMoreItemThanStackLimit<T>(MyStack<T> stack, int limit, T value)
        {
            for (int i = 0; i <= limit; i++)
            {
                stack.Push(value);
            }
        }

        [Test]
        public void Peek_WhenCalled_ReturnTheRightValue()
        {
            MyStack<int> intStack = new MyStack<int>(3);

            intStack.Push(1);
            intStack.Push(2);
            intStack.Push(3);

            Assert.That(intStack.Peek(), Is.EqualTo(3));
        }

        [Test]
        public void Peek_WhenCalled_ThrowExceptionWhenStackIsEmpty()
        {
            MyStack<int> myStack = new MyStack<int>(3);

            Assert.Catch(() => myStack.Peek());
        }

        [Test]
        public void Pop_WhenCalled_ReturnTheTopValueAndRemoveItFromStack()
        {
            MyStack<int> intStack = new MyStack<int>(3);

            intStack.Push(1);
            intStack.Push(2);
            intStack.Push(3);

            Assert.That(intStack.Pop(), Is.EqualTo(3));
            Assert.That(intStack.Count, Is.EqualTo(2));
        }

        [Test]
        public void Pop_WhenCalled_ThrowANewExceptionWhenStackIsEmpty()
        {
            MyStack<Person> myStack = new MyStack<Person>(3);
            
            Assert.Catch(() => myStack.Pop());
        }

        [Test]
        public void Clear_WhenCalled_ClearAllItemsInStack()
        {
            MyStack<int> intStack = new MyStack<int>(3);

            intStack.Push(1);
            intStack.Push(2);
            intStack.Push(3);

            intStack.Clear();
            Assert.That(intStack.IsEmpty, Is.EqualTo(true));
        }
    }
}