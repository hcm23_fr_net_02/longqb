﻿using DemoStackAndQueue;

class Program
{
    static void Main()
    {
        Console.WriteLine("--------- Demo stack ---------");
        MyStack myStack = new MyStack();

        Console.WriteLine("Add 3 values 1, 2, 3 to stack");
        myStack.Push(1);
        myStack.Push(2);
        myStack.Push(3);

        Console.WriteLine("Get the number of elements in stack: {0}", myStack.Count);
        Console.WriteLine("Peek the top number in stack: {0}", myStack.Peek());

        Console.WriteLine("Pop the top number in stack: {0}", myStack.Pop());
        Console.WriteLine("Get the number of elements in stack after pop: {0}", myStack.Count);
        Console.WriteLine("Peek the top number in stack after pop: {0}", myStack.Peek());


        Console.WriteLine("--------- Demo queue ---------");
        MyQueue myQueue = new MyQueue();

        Console.WriteLine("Add 3 values 1, 2, 3 to queue");
        myQueue.Push(1);
        myQueue.Push(2);
        myQueue.Push(3);

        Console.WriteLine("Get the number of elements in queue: {0}", myQueue.Count);
        Console.WriteLine("Peek the top number in queue: {0}", myQueue.Peek());

        Console.WriteLine("Pop the top number in queue: {0}", myQueue.Pop());
        Console.WriteLine("Get the number of elements in queue after pop: {0}", myQueue.Count);
        Console.WriteLine("Peek the top number in queue after pop: {0}", myStack.Peek());

    }
}