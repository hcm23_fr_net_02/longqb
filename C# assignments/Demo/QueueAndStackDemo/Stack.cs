﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoStackAndQueue
{
    public class MyStack
    {
        private int length;
        private int topIndex;
        private int[] arr;

        public int Count { get { return length; } }

        public MyStack()
        {
            length = 0;
            topIndex = -1;
            arr = new int[100];
        }

        public void Push(int item)
        {
            length++;
            topIndex++;
            arr[topIndex] = item;
        }

        public int Pop()
        {
            int result = arr[topIndex];
            arr[topIndex--] = 0;
            length--;
            return result;
        }

        public int Peek()
        {
            return arr[topIndex];
        }
    }
}
