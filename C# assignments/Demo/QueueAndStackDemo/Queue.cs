﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoStackAndQueue
{
    public class MyQueue
    {
        private int length;
        private int topIndex;
        private int[] arr;

        public int Count { get { return length; } }

        public MyQueue()
        {
            length = 0;
            topIndex = -1;
            arr = new int[100];
        }

        public void Push(int item)
        {
            length++;
            topIndex++;
            arr[topIndex] = item;
        }

        public int Pop()
        {
            int result = arr[0];
            int[] tmp = new int[length];
            for (int i = 1; i < length; i++)
            {
                tmp[i - 1] = arr[i];
            }
            topIndex--;
            length--;
            arr = tmp;
            return result;
        }

        public int Peek()
        {
            return arr[0];
        }
    }
}
