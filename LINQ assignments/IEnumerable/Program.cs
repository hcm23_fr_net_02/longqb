﻿using System;
using System.Threading.Channels;

// Fruit
var fruits = new List<string>()
{
    "apple",
    "banana",
    "orange",
    "pineapple",
    "mango"
};

// c1.1
var f1  = fruits.Select(x => x.ToUpper());
foreach (var c in f1)
{
    Console.WriteLine(c);
}
Console.WriteLine();

// c1.2
var f2 = fruits.Select(x => $"{x}:{x.Length}");
foreach (var c in f2)
{
    Console.WriteLine(c);
}
Console.WriteLine();

// c1.3
var f3 = fruits.Select(x => x[0]);
foreach (var c in f3)
{
    Console.WriteLine(c);
}
Console.WriteLine();

// c1.4
var f4 = fruits.Select(x => string.Join("", x.ToCharArray().Reverse()));
foreach(var c in f4)
{
    Console.WriteLine(c);
}
Console.WriteLine();

var f5 = fruits.Where(x => x.Length == 6);

var f6 = fruits.Where(x => x.StartsWith('a'));

var f7 = fruits.Where(x => x.Contains('a'));

// Dates
List<DateTime> dates = new List<DateTime>()
{
    new DateTime(2021, 1, 1),
    new DateTime(2022, 2, 1),
    new DateTime(2023, 3, 1),
    new DateTime(2024, 4, 1),
    new DateTime(2025, 5, 1),
};

var d1 = dates.Select(x => x.Month);
foreach (var c in d1)
{
    Console.WriteLine(c);
}
Console.WriteLine();

var d2 = dates.Select(x => x.ToString("dd-MM-yyyy"));
foreach (var c in d2)
{
    Console.WriteLine(c);
}
Console.WriteLine();

dates = new List<DateTime>()
{
    new DateTime(2021, 1, 1),
    new DateTime(2022, 2, 1),
    new DateTime(2023, 1, 7),
    new DateTime(2024, 1, 5),
    new DateTime(2025, 5, 10),
};

var d3 = dates.Where(x => x.Year == 2023);

var d4 = dates.Where(x => x.Month == 1 && x.Year == 2024);

var d5 = dates.Where(x => x.Month == 5 && x.Day == 10);

dates = new List<DateTime>
{
    new DateTime(2021, 1, 15),
    new DateTime(2022, 5, 10),
    new DateTime(2020, 12, 1),
    new DateTime(2023, 3, 25),
    new DateTime(2024, 8, 5)
};

var d6 = dates.OrderBy(x => x);

// Employees
var employees = new List<Employee>
{
    new Employee { Name = "Alice", Department = "Sales", Salary = 50000 },
    new Employee { Name = "Bob", Department = "Marketing", Salary = 60000 },
    new Employee { Name = "Charlie", Department = "Sales", Salary = 55000 },
    new Employee { Name = "David", Department = "IT", Salary = 70000 }
};

// c3_1
var e1 = employees.Select(x => x.Name);
foreach (var c in e1)
{
    Console.WriteLine(c);
}
Console.WriteLine();

// c3_2
var e2 = employees.Select(x => $"{x.Name} - {x.Department}");
foreach (var c in e2)
{
    Console.WriteLine(c);
}
Console.WriteLine();

// c3_3
var e3 = employees.Select(x => string.Join("", x.Name.ToCharArray().Reverse()));
foreach (var c in e3)
{
    Console.WriteLine(c);
}
Console.WriteLine();

var e4 = employees.Where(x => x.Department == "IT");

var e5 = employees.Where(x => x.Salary > 60000);

var e6 = employees.Where(x => x.Name.Contains('o') || x.Name.EndsWith('E'));

// Names
var names = new List<string> {
    "John",
    "Alice",
    "Bob",
    "David",
    "Emily"
};

var n1 = names.OrderBy(x => x);
foreach (var item in n1)
{
    Console.WriteLine(item);
}
Console.WriteLine();

List<Person> people = new List<Person>
{
    new Person { Name = "Alice", Age = 25 },

    new Person { Name = "Bob", Age = 30 },

    new Person { Name = "Charlie", Age = 22 },

    new Person { Name = "David", Age = 28 },

    new Person { Name = "Emily", Age = 27 }
};

var p1 = people.OrderBy(x => x.Age);

List<int> numbers = new List<int> { 3, 8, 1, 5, 2, 10, 7 };

var number1 = numbers.Where(x => x % 2 == 0).OrderBy(x => x);

var number2 = numbers.Where(x => x % 2 != 0).OrderByDescending(x => x);

var number3 = numbers.Where(x => x > 5 && x < 10).OrderBy(x => x);

List<Product> products = new List<Product>
{
    new Product { Id = 1, Name = "Laptop", Price = 1000 },
    new Product { Id = 2, Name = "Phone", Price = 500 },
    new Product { Id = 3, Name = "Headphones", Price = 50 },
    new Product { Id = 4, Name = "Mouse", Price = 20 },
    new Product { Id = 5, Name = "Keyboard", Price = 30 }
};

var product1 = products.Where(x => x.Price > 100).OrderByDescending(x => x.Price);

var product2 = products.Where(x => x.Name.Contains('o')).OrderBy(x => x.Name);

class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public decimal Price { get; set; }
}

class Person
{
    public string Name { get; set; }
    public int Age { get; set; }
}

class Employee
{
    public string Name { get; set; }
    public string Department { get; set; }
    public decimal Salary { get; set; }
}
