﻿// Hãy giả định chúng ta có một danh sách các sản phẩm của một cửa hàng với các thuộc tính sau: 
// tên sản phẩm, giá, và danh mục (category). 
// Hãy tạo một danh sách các sản phẩm và thực hiện các yêu cầu sau bằng cách sử dụng Linq:

// Tạo danh sách các sản phẩm:
var products = new List<Product>
{
    new Product { Name = "Áo sơ mi", Price = 300000, Category = "Áo" },
    new Product { Name = "Quần jeans", Price = 500000, Category = "Quần" },
    new Product { Name = "Giày thể thao", Price = 800000, Category = "Giày" },
    new Product { Name = "Ví da", Price = 200000, Category = "Phụ kiện" },
    new Product { Name = "Mũ nón", Price = 100000, Category = "Phụ kiện" },
    new Product { Name = "Đầm dạ hội", Price = 1500000, Category = "Đầm" },
    // Thêm các sản phẩm khác tại đây...
};

// Tìm danh sách các sản phẩm có giá lớn hơn 500,000 đồng:
var productsWithPriceAbove500000 = products.Where(x => x.Price > 500000);
//Console.WriteLine()


// Lấy ra tên của các sản phẩm có giá nhỏ hơn 300,000 đồng và sắp xếp theo thứ tự giảm dần của giá:
var productsWithPriceBelow300000 = products.Where(x => x.Price < 300000).OrderByDescending(x => x.Price);


// Tạo một dictionary với danh mục sản phẩm là key và danh sách các sản phẩm trong cùng danh mục là value:
var dictionaryWithKeyIsCategory = products.GroupBy(x => x.Category);



// Tính tổng giá tiền của các sản phẩm trong danh mục "Áo":
var sumOfProductPriceWithCategoryAo = products.Where(x => x.Category.Equals("Áo")).Sum(x => x.Price);


// Tìm sản phẩm có giá cao nhất:
var productWithHighestPrice = products.Max(x => x.Price);


// Tạo danh sách các sản phẩm với tên đã viết hoa:
var productWithUpperName = products.Select(x => new Product { 
    Category = x.Category,
    Name = x.Name.ToUpper(),
    Price = x.Price
});

class Product
{
    public string Name { get; set; }
    public int Price { get; set; }
    public string Category { get; set; }
}
