﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelectWhereOrderBy
{
    public class Employee
    {
        private int _id;
        private string _lastName;
        private string _firstName;
        private string _email;
        private string _gender;
        private string _country;
        private string _postalCode;
        private string _jobTitle;
        private DateTime? _startDate;
        private double _salary;

        public Employee(int id,
            string lastName,
            string firstName,
            string email,
            string gender,
            string country,
            string postalCode,
            string jobTitle,
            DateTime? startDate,
            double salary)
        {
            _id = id;
            _lastName = lastName;
            _firstName = firstName;
            _email = email;
            _gender = gender;
            _country = country;
            _postalCode = postalCode;
            _jobTitle = jobTitle;
            _startDate = startDate;
            _salary = salary;
        }

        public int Id { get { return _id; } }
        public string LastName { get { return _lastName; } }
        public string FirstName { get { return _firstName; } }
        public string Email { get { return _email; } }
        public string Gender { get { return _gender; } }
        public string Country { get { return _country; } }
        public string PostalCode { get { return _postalCode; } }
        public string JobTitle { get { return _jobTitle; } }
        public DateTime? StartDate { get { return _startDate; } }
        public double Salary { get { return _salary; } }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
