﻿using Newtonsoft.Json;
using SelectWhereOrderBy;
using System.Text.Json.Serialization;

var mockDataFilePath = "D:\\.NET project practice\\SelectWhereOrderBy\\SelectWhereOrderBy\\mock-data.json";
string data = await File.ReadAllTextAsync(mockDataFilePath);


List<Employee> employees = JsonConvert.DeserializeObject<List<Employee>>(data);

var employeesFirstNames = employees.Select(x => x.FirstName);

var employeesWithSalaryAbove500000 = employees.Where(x => x.Salary > 50000);

var maleEmployees = employees.Where(x => x.Gender == "Male").OrderBy(x => x.FirstName);

var indonesiaManagers = employees.Where(x => x.Country == "Indonesia")
    .Where(x => x.JobTitle is not null && x.JobTitle.Contains("Manager"));

var employeesWhoHasEmail = employees.Where(x => x.Email is not null).OrderByDescending(x => x.LastName);

var employeesStartBefore20220101AndSalaryAbove60000 = employees
    .Where(x => x.Salary > 60000 && x.StartDate < DateTime.Parse("2022-01-01"));

var employeesWithoutPostalCode = employees.Where(x => string.IsNullOrEmpty(x.PostalCode))
    .OrderBy(x => x.LastName);

var employeesWithinSalaryRange = employees.Where(x => x.Salary >= 50000 && x.Salary <= 70000)
    .OrderBy(x => x.Salary);

var employeesWithFirstNameStartWithA = employees.Where(x => x.FirstName.ToLower()[0] == 'a')
    .OrderByDescending(x => x.LastName);

foreach (var e in employeesStartBefore20220101AndSalaryAbove60000)
{
    Console.WriteLine(e.ToString());
}