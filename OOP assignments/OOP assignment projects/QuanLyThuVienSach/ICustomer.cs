﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyThuVienSach
{
    public interface ICustomer
    {
        public string CustomerName { get; }
        public string CustomerAddress { get; }  
        public int CustomerId { get; }
    }
}
