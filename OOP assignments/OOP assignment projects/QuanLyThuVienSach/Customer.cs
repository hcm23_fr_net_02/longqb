﻿namespace QuanLyThuVienSach
{
    public class Customer : ICustomer
    {
        private string _customerName;
        private string _customerAddress;
        private int _customerId;

        private static int _countId = 1;
        public string CustomerName { get => _customerName; }
        
        public string CustomerAddress { get => _customerAddress; }

        public int CustomerId { get => _customerId; }

        public Customer(string customerName, string customerAddress)
        {
            _customerName = customerName;
            _customerAddress = customerAddress;
            _customerId = _countId++;
        }
    }
}
