﻿namespace QuanLyThuVienSach
{
    public class Library: ILibrary
    {
        private IList<IBook> _listOfBooks;
        private IList<ILoan> _listOfLoan;

        public Library()
        {
            _listOfBooks = new List<IBook>();
            _listOfLoan = new List<ILoan>();
        }

        public bool AddNewBook(IBook book)
        {
            try
            {
                var isExistInLibrary = _listOfBooks.Contains(book);
                if (isExistInLibrary)
                {
                    throw new Exception("This book has already been existed in the library");
                }
                _listOfBooks.Add(book);
                return true;
            } catch(Exception)
            {
                throw;
            }
        }

        public IBook FindBookByID(int id)
        {
            try
            {
                var book = _listOfBooks.FirstOrDefault(x => x.Id == id);
                if (book is null)
                {
                    throw new Exception("Cannot found the book with this id");
                }
                return book;
            }
            catch(Exception)
            {
                throw;
            }
        }

        public IList<IBook> GetAllBooks()
        {
            return _listOfBooks;
        }

        public IList<ILoan> GetAllLoans()
        {
            return _listOfLoan;
        }

        public bool LoanBook(IBook book, Customer customer, DateTime returnDate)
        {
            try
            {
                var resultBook = _listOfBooks.FirstOrDefault(x => x == book);

                if (resultBook is null)
                {
                    throw new Exception("Cannot find this book");
                }

                if (customer is null)
                {
                    throw new Exception("Invalid customer");
                }

                if (resultBook.NumberOfRemainingBooks <= 0)
                {
                    throw new Exception("This book has already been lent. " +
                        "Please choose another book");
                }

                resultBook.DecreaseRemainingBooks();
                _listOfLoan.Add(new Loan(DateTime.Now, returnDate, customer));

                return true; 
            }
            catch(Exception)
            {
                throw;
            }
        }

        public bool RemoveBook(IBook book)
        {
            try
            {
                return _listOfBooks.Remove(book);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
