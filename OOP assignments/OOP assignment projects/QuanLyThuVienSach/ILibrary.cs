﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyThuVienSach
{
    public interface ILibrary
    {
        public bool AddNewBook(IBook book);
        public bool RemoveBook(IBook book);
        public IList<IBook> GetAllBooks();
        public bool LoanBook(IBook book, Customer customer, DateTime returnDate);
        public IList<ILoan> GetAllLoans();
        public IBook FindBookByID(int id);
    }
}
