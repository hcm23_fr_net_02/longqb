﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyThuVienSach
{
    public interface ILoan
    {
        public int LoanId { get; }
        public DateTime BorrowDate { get; }
        public DateTime ReturnDate { get; }
        public IList<IBook> LoanBooks { get; }
        public Customer Customer { get; }
    }
}
