﻿namespace QuanLyThuVienSach
{
    public class Loan : ILoan
    {
        private int _loanId;
        private DateTime _borrowDate;
        private DateTime _returnDate;
        private Customer _customer;
        private IList<IBook> _loanBooks;

        private static int _countId = 1;
        public int LoanId { get => _loanId; }

        public DateTime BorrowDate { get => _borrowDate; }

        public DateTime ReturnDate {  get => _returnDate; }

        public IList<IBook> LoanBooks { get => _loanBooks; }

        public Customer Customer { get => _customer; }

        public Loan(DateTime borrowDate, DateTime returnDate, Customer customer)
        {
            _borrowDate = borrowDate;
            _returnDate = returnDate;
            _loanBooks = new List<IBook>();
            _loanId = _countId++;
            _customer = customer;
        }
    }
}
