﻿namespace QuanLyThuVienSach
{
    public class Book : IBook
    {
        private int _id;
        private string _title;
        private string _author;
        private int _numberOfRemainingBooks;
        private double _price;

        private static int _countId = 1;

        public int Id { get { return _id; } }
        public string Title { get { return _title; } }
        public string Author { get { return _author; } }
        public int NumberOfRemainingBooks {  get { return _numberOfRemainingBooks; } }
        public double Price { get { return _price; } }
        

        public Book(string title, string author, int numberOfRemainingBook,
            double price)
        {
            _author = author;
            _id = _countId++;
            _title = title;
            _numberOfRemainingBooks = numberOfRemainingBook;
            _price = price;
        }

        public bool DecreaseRemainingBooks()
        {
            if (_numberOfRemainingBooks > 0)
            {
                _numberOfRemainingBooks--;
                return true;
            }
            return false;
        }
    }
}
