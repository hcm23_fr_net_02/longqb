﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyThuVienSach
{
    public interface IBook
    {
        public int Id { get; }
        public string Title { get; }
        public string Author { get; }
        public int NumberOfRemainingBooks {  get; }
        public double Price { get; }
        public bool DecreaseRemainingBooks();
    }
}
