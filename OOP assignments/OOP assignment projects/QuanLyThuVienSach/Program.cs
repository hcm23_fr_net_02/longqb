﻿using QuanLyThuVienSach;

var library = new Library();    
int option;

do
{
    Console.WriteLine(Constant.MENU);
    Console.WriteLine(Constant.FIRST_OPTION);
    Console.WriteLine(Constant.SECOND_OPTION);
    Console.WriteLine(Constant.THIRD_OPTION);
    Console.WriteLine(Constant.FOURTH_OPTION);
    Console.WriteLine(Constant.FIFTH_OPTION);
    Console.WriteLine(Constant.SIXTH_OPTION);
    Console.WriteLine(Constant.SEVENTH_OPTION);
    Console.WriteLine(Constant.END_MENU_CHARACTERS);
    Console.Write(Constant.CHOOSE_OPTION);
    int.TryParse(Console.ReadLine(), out option);

    try
    {
        switch(option)
        {

            case 1:
                Console.WriteLine(Constant.ENTER_BOOK_NAME);
                string bookName = Console.ReadLine();
                Console.WriteLine(Constant.ENTER_AUTHOR_NAME);
                string authorName = Console.ReadLine();
                double price;
                do
                {
                    Console.WriteLine(Constant.ENTER_PRICE);
                    double.TryParse(Console.ReadLine(), out price);

                    if (price <  0)
                    {
                        Console.WriteLine(Constant.INVALID_INPUT);
                    }
                    else
                    {
                        break;
                    }

                } while (true);

                int numberOfBooks;
                do
                {
                    Console.WriteLine(Constant.ENTER_NUMBER_OF_STOCK_IN_STORAGE);
                    int.TryParse(Console.ReadLine(), out numberOfBooks);

                    if (numberOfBooks <= 0)
                    {
                        Console.WriteLine(Constant.INVALID_INPUT);
                        continue;
                    }
                    break;

                } while (true);

                bool returningResult = library.AddNewBook(
                    new Book(bookName, authorName, numberOfBooks, price));

                if (returningResult)
                {
                    Console.WriteLine(Constant.SUCCESSFUL);
                }
                else
                {
                    Console.WriteLine(Constant.FAIL);
                }

                break;

            case 2:
                Console.WriteLine(Constant.ENTER_BOOK_ID);
                int id;
                int.TryParse(Console.ReadLine(), out id);
                var bookNeedToDelete = library.FindBookByID(id);
                returningResult = library.RemoveBook(bookNeedToDelete);

                if (returningResult)
                {
                    Console.WriteLine(Constant.SUCCESSFUL);
                }
                else
                {
                    Console.WriteLine(Constant.FAIL);
                }

                break;

            case 3:
                var listOfBooksInLibrary = library.GetAllBooks();

                foreach(var book in listOfBooksInLibrary)
                {
                    Console.WriteLine($"{book.Id} - {book.Title} - {book.Author} - " +
                        $"{book.Price} - {book.NumberOfRemainingBooks}");
                }

                break;

            case 4:
                Console.WriteLine(Constant.ENTER_CUSTOMER_NAME);
                string customerName = Console.ReadLine();
                Console.WriteLine(Constant.ENTER_CUSTOMER_ADDRESS);
                string customerAddress = Console.ReadLine();
                Console.WriteLine(Constant.ENTER_BOOK_ID);
                int.TryParse(Console.ReadLine(), out id);
                var bookNeedToFind = library.FindBookByID(id);

                if (bookNeedToFind is null)
                {
                    Console.WriteLine(Constant.FAIL);
                    break;
                }

                DateTime returnDate;
                Console.WriteLine(Constant.ENTER_RETURN_DATE);
                var result = DateTime.TryParse(Console.ReadLine(), out returnDate);

                if (!result)
                {
                    Console.WriteLine(Constant.FAIL);
                    break;
                }

                returningResult = library.LoanBook(bookNeedToFind,
                    new Customer(customerName, customerAddress),
                    returnDate);

                if (!returningResult)
                {
                    Console.WriteLine(Constant.FAIL);
                    break;
                }
                else
                {
                    Console.WriteLine(Constant.SUCCESSFUL);
                    break;
                }

            case 5:
                var loanBooks = library.GetAllLoans();

                foreach(var loan in loanBooks)
                {
                    Console.WriteLine($"{loan.LoanId} - {loan.BorrowDate} - {loan.ReturnDate}" +
                        $"{loan.Customer.CustomerName} - " +
                        $"{string.Join(',', loan.LoanBooks.Select(x => x.Title))}");
                }

                break;

            case 6:
                int.TryParse(Console.ReadLine(), out id);
                bookNeedToFind = library.FindBookByID(id);

                if (bookNeedToFind is not null)
                {
                    Console.WriteLine($"{bookNeedToFind.Id} - {bookNeedToFind.Title} " +
                        $"- {bookNeedToFind.Author} - {bookNeedToFind.Price}" +
                        $"{bookNeedToFind.NumberOfRemainingBooks}");
                }
                else
                {
                    Console.WriteLine(Constant.FAIL);
                }

                break;

            case 7:
                break;

            default:
                break;
        }
    }
    catch(Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (option != 7);