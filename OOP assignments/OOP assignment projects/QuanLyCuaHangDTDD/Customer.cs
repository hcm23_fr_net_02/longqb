﻿namespace QuanLyCuaHangDTDD
{
    public class Customer
    {
        private string _name;
        private string _address;
        private int _id;
        private static int _countID = 1;
        public string Name { get => _name; }
        public string Address { get => _address; }
        public int Id { get => _id; }

        public Customer(string name, string address) 
        { 
            _name = name;
            _address = address;
            _id = _countID++;
        }
    }
}
