﻿using QuanLyCuaHangDTDD;

Console.OutputEncoding = System.Text.Encoding.UTF8;
List<Customer> list = new List<Customer>()
{
    new Customer("Adam", "Stand Street"),
    new Customer("Nolan", "Gray Street"),
    new Customer("Will", "Urban Street"),
};
Store store = new Store();

int option;
do
{
    Console.WriteLine(Constant.MENU);
    Console.WriteLine(Constant.FIRST_OPTION);
    Console.WriteLine(Constant.SECOND_OPTION);
    Console.WriteLine(Constant.THIRD_OPTION);
    Console.WriteLine(Constant.FOURTH_OPTION);
    Console.WriteLine(Constant.FIFTH_OPTION);
    Console.WriteLine(Constant.SIXTH_OPTION);
    Console.WriteLine(Constant.SEVENTH_OPTION);
    Console.WriteLine(Constant.END_MENU_CHARACTERS);
    Console.Write(Constant.CHOOSE_OPTION);
    int.TryParse(Console.ReadLine(), out option);

    switch(option)
    {
        case 1:
            Console.WriteLine(Constant.ENTER_PHONE_NAME);
            string phoneName = Console.ReadLine();
            Console.WriteLine(Constant.ENTER_PHONE_PRODUCER);
            string phoneProducer = Console.ReadLine();
            Console.WriteLine(Constant.ENTER_PRICE);
            double phonePrice;
            double.TryParse(Console.ReadLine(), out phonePrice);
            Console.WriteLine(Constant.ENTER_NUMBER_OF_STOCK_IN_STORAGE);
            int stockInStorage;
            int.TryParse(Console.ReadLine(), out stockInStorage);
            bool returningResult = store.AddNewProduct(
                new MobilePhone(phoneName, phoneProducer, phonePrice, stockInStorage));
            if (returningResult)
            {
                Console.WriteLine(Constant.SUCCESSFUL);
            }
            else
            {
                Console.WriteLine(Constant.FAIL);
            }
            break;
        case 2:
            Console.WriteLine(Constant.ENTER_PHONE_ID);
            int phoneId;
            int.TryParse(Console.ReadLine(), out phoneId);
            returningResult = store.RemoveProduct(store.GetProduct(phoneId));
            if (returningResult)
            {
                Console.WriteLine(Constant.SUCCESSFUL);
            }
            else
            {
                Console.WriteLine(Constant.FAIL);
            }
            break;
        case 3:
            var listOfProducts = store.GetAllProducts();
            foreach(var product in listOfProducts)
            {
                Console.WriteLine($"{product.PhoneId} - {product.PhoneName} - " +
                    $"{product.Price} - {product.PhoneProducer} - {product.StockInStorage}");
            }
            break;
        case 4:
            Console.WriteLine(Constant.ENTER_CUSTOMER_NAME);
            string customerName = Console.ReadLine();
            Console.WriteLine(Constant.ENTER_CUSTOMER_ADDRESS);
            string customerAddress = Console.ReadLine();
            Console.WriteLine(Constant.ENTER_PHONE_ID);
            int.TryParse(Console.ReadLine(), out phoneId);
            returningResult = store.BuyProduct(
                new Customer(customerName, customerAddress), 
                phoneId);
            if (returningResult)
            {
                Console.WriteLine(Constant.SUCCESSFUL);
            }
            else
            {
                Console.WriteLine(Constant.FAIL);
            }
            break;
        case 5:
            var historyPurchase = store.GetAllPurchases();
            foreach (var purchase in historyPurchase)
            {
                Console.WriteLine($"{purchase.PurchaseID} - {purchase.Customer.Name} - " +
                    $"{purchase.BoughtDay}");
            }
            break;
        case 6:
            Console.WriteLine(Constant.ENTER_PHONE_ID);
            int.TryParse(Console.ReadLine(), out phoneId);
            var phone = store.GetProduct(phoneId);
            Console.WriteLine($"{phone.PhoneId} - {phone.PhoneName} - " +
                    $"{phone.Price} - {phone.PhoneProducer} - {phone.StockInStorage}");
            break;
        case 7:
            break;
        default:
            Console.WriteLine("Invalid option");
            break;
    }
} while (option != 7);

