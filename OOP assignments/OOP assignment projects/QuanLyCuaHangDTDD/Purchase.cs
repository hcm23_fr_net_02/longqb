﻿namespace QuanLyCuaHangDTDD
{
    public class Purchase
    {
        private int _purchaseID;
        private DateTime _boughtDay;
        private List<MobilePhone> _phones;
        private Customer _customer;
        private static int _count = 0;

        public int PurchaseID { get { return _purchaseID; } }
        public DateTime BoughtDay { get { return _boughtDay; } }
        public List<MobilePhone> Phones { get {  return _phones; } }
        public Customer Customer { get { return _customer; } }

        public Purchase(DateTime dateTime, Customer customer) 
        { 
            _purchaseID = ++_count;
            _boughtDay = dateTime;
            _phones = new List<MobilePhone>();
            _customer = customer;
        }

        public void AddNewPhonePurchase(MobilePhone phone)
        {
            _phones.Add(phone);
        }
    }
}
