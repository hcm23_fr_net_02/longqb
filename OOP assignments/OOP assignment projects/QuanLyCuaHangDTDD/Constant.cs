﻿
public static class Constant
{
    public const string MENU = "********* MENU PHONE STORE MANAGEMENT *********";
    public const string FIRST_OPTION = "1. Add new phone";
    public const string SECOND_OPTION = "2. Remove a phone from the store";
    public const string THIRD_OPTION = "3. Show all phones info";
    public const string FOURTH_OPTION = "4. Allow customer to buy a phone";
    public const string FIFTH_OPTION = "5. Show all history purchase";
    public const string SIXTH_OPTION = "6. Find phone by id";
    public const string SEVENTH_OPTION = "7. Exit";
    public const string END_MENU_CHARACTERS = "**************************************";
    public const string CHOOSE_OPTION = "Choose option: ";

    public const string ENTER_PHONE_NAME = "Enter phone name: ";
    public const string ENTER_PHONE_PRODUCER = "Enter phone producer: ";
    public const string ENTER_NUMBER_OF_STOCK_IN_STORAGE = "Enter the number of stocks in storage: ";
    public const string ENTER_PRICE = "Enter the price of the phone: ";
    public const string ENTER_PHONE_ID = "Enter phone id: ";

    public const string ENTER_CUSTOMER_NAME = "Enter customer's name: ";
    public const string ENTER_CUSTOMER_ADDRESS = "Enter customer's address: ";

    public const string SUCCESSFUL = "Successful";
    public const string FAIL = "Fail";

    public const string ERROR = "ERROR";

}