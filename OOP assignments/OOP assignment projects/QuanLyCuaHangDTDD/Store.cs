﻿namespace QuanLyCuaHangDTDD
{
    public class Store
    {
        private List<MobilePhone> _products;
        private List<Purchase> _purchases;

        public Store() 
        { 
            _products = new List<MobilePhone>();
            _purchases = new List<Purchase>();
        }

        public bool AddNewProduct(MobilePhone product)
        {
            if (_products.FirstOrDefault(x => x.PhoneId == product.PhoneId) is not null)
            {
                return false;
            }
            _products.Add(product);
            return true;
        }

        public bool BuyProduct(Customer customer, int id)
        {
            var product = _products.FirstOrDefault(x => x.PhoneId == id
                && x.StockInStorage > 0);
            if (product is not null)
            {
                product.DecreaseStock();
                var purchase = _purchases.FirstOrDefault(x => x.Customer.Id == customer.Id);
                if (purchase is null)
                {
                    purchase = new Purchase(DateTime.Today, customer);
                }
                purchase.AddNewPhonePurchase(product);
                _purchases.Add(purchase);
                return true;
            }
            return false;
        }

        public List<MobilePhone> GetAllProducts()
        {
            return _products;
        }

        public List<Purchase> GetAllPurchases()
        {
            return _purchases;
        }

        public MobilePhone GetProduct(int id)
        {
            return _products.FirstOrDefault(x => x.PhoneId == id);
        }

        public bool RemoveProduct(MobilePhone product)
        {
            return _products.Remove(product);
        }
    }
}
