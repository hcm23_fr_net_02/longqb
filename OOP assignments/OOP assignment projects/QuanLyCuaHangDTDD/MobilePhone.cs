﻿namespace QuanLyCuaHangDTDD
{
    public class MobilePhone
    {
        private string _phoneName;
        private string _phoneProducer;
        private int _stockInStorage;
        private double _price;
        private int _phoneId;
        private static int _countId = 1;

        public string PhoneName { get => _phoneName; }
        public string PhoneProducer { get => _phoneProducer; }
        public int StockInStorage { get => _stockInStorage; }
        public double Price { get => _price; }
        public int PhoneId { get => _phoneId; }  

        public MobilePhone(string name, string producer, double price, int stockInStorage)
        {
            _phoneName = name;
            _phoneProducer = producer;
            _stockInStorage = stockInStorage;
            _price = price;
            _phoneId = _countId++;
        }

        public void DecreaseStock()
        {
            _stockInStorage--;
        }
    }
}
