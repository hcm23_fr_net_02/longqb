﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyKhachSan
{
    public class Invoice
    {
        private int _id;
        private List<IService> _listOfServices;
        private Booking _booking;
        private static int _countId = 1;

        public int Id { get { return _id; } }
        public double TotalPrice
        {
            get
            {
                var numberOfDays = DateTime.Now.Day - _booking.ReceivingDate.Day;
                return _booking.BookingRooms.Sum(x => x.PricePerNight * numberOfDays)
                    + _listOfServices.Sum(x => x.ServicePrice);
            }
        }

        public Booking BookingDetail { get { return _booking; } }
        public List<IService> ListServices { get { return _listOfServices; } }   

        public Invoice(List<IService> listOfServices, Booking booking)
        {
            _id = _countId++;
            this._listOfServices = listOfServices;
            _booking = booking;
        }
    }
}
