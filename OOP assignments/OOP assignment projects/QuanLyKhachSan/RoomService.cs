﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyKhachSan
{
    public class RoomService : IService
    {
        private double _servicePrice;

        public double ServicePrice
        {
            get { return _servicePrice; }
        }

        public RoomService(double servicePrice)
        {
            _servicePrice = servicePrice;
        }
    }
}
