﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyKhachSan
{
    public class Room
    {
        private int _id;
        private string _type;
        private double _pricePerNight;
        private bool _isBooked;
        private string _conveniences;
        private static int _countId = 1;

        public int Id { get { return _id; } }
        public string Type { get { return _type;} }
        public double PricePerNight { get { return _pricePerNight; } }
        public bool IsBook { get { return _isBooked; } set => _isBooked = value; }   
        public string Conveniences { get { return _conveniences; } }

        public Room(string type, double pricePerNight, bool isBooked, 
            string conveniences)
        {
            _id = _countId++;
            _type = type;
            _pricePerNight = pricePerNight;
            _isBooked = isBooked;
            _conveniences = conveniences;
        }
    }
}
