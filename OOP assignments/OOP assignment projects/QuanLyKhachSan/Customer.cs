﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyKhachSan
{
    public class Customer
    {
        private int _id;
        private string _name;
        private string _phoneNumber;
        private string _address;
        private int _countId = 1;

        public int Id { get { return _id; } }
        public string Name { get { return _name;} }
        public string PhoneNumber { get { return _phoneNumber;} }   
        public string Address { get { return _address;} }

        public Customer(string name, string phoneNumber, string address)
        {
            _id = _countId++;
            _name = name;
            _phoneNumber = phoneNumber;
            _address = address;
        }
    }
}
