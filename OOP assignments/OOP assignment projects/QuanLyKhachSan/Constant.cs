﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyKhachSan
{
    public class Constant
    {
        public const string MENU = "********* MENU HOTEL MANAGEMENT *********";
        public const string FIRST_OPTION = "1. Add new room";
        public const string SECOND_OPTION = "2. Remove a room";
        public const string THIRD_OPTION = "3. Show all rooms' info";
        public const string FOURTH_OPTION = "4. Book rooms";
        public const string FIFTH_OPTION = "5. Show all current booked rooms";
        public const string SIXTH_OPTION = "6. Generate invoice";
        public const string SEVENTH_OPTION = "7. Exit";
        public const string END_MENU_CHARACTERS = "**************************************";
        public const string CHOOSE_OPTION = "Choose option: ";
    }
}
