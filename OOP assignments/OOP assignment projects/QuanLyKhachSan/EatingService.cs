﻿namespace QuanLyKhachSan
{
    public class EatingService : IService
    { 

        private double _servicePrice;
        public double ServicePrice
        {
            get { return _servicePrice; }
        }

        public EatingService(double price)
        {
            _servicePrice = price;
        }
    }
}
