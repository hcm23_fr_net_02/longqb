﻿using QuanLyKhachSan;

Hotel hotel = new Hotel();
int option;

do
{
    Console.WriteLine(Constant.MENU);
    Console.WriteLine(Constant.FIRST_OPTION);
    Console.WriteLine(Constant.SECOND_OPTION);
    Console.WriteLine(Constant.THIRD_OPTION);
    Console.WriteLine(Constant.FOURTH_OPTION);
    Console.WriteLine(Constant.FIFTH_OPTION);
    Console.WriteLine(Constant.SIXTH_OPTION);
    Console.WriteLine(Constant.SEVENTH_OPTION);
    Console.WriteLine(Constant.END_MENU_CHARACTERS);
    Console.Write(Constant.CHOOSE_OPTION);

    int.TryParse(Console.ReadLine(), out option);

    try
    {
        switch(option)
        {
            case 1:
                Console.WriteLine("Enter room's id: ");
                int roomId;
                int.TryParse(Console.ReadLine(), out roomId);
                Console.WriteLine("Enter room's type (Single bedroom, double bedroom, or " +
                    "family room): ");
                string roomType = Console.ReadLine();
                double price;
                Console.WriteLine("Enter room's price per night: ");
                double.TryParse(Console.ReadLine(), out price);
                Console.WriteLine("Enter conveniences: ");
                string conveniences = Console.ReadLine();

                hotel.AddNewRoom(roomType, price, false, conveniences);
                break;

            case 2:
                Console.WriteLine("Enter room's id: ");
                int.TryParse(Console.ReadLine(), out roomId);
                hotel.RemoveRoom(roomId);
                break;

            case 3:
                hotel.ShowAllRoomDetails();
                break;

            case 4:
                int numberOfRooms;
                List<int> roomIds = new List<int>();
                Console.WriteLine("Enter the number of rooms you want to book: ");
                int.TryParse(Console.ReadLine(), out numberOfRooms);

                for(int i = 0; i < numberOfRooms; i++)
                {
                    int id;
                    Console.WriteLine("Enter the room's id which you want to book: ");
                    int.TryParse(Console.ReadLine(), out id);
                    roomIds.Add(id);
                }

                hotel.BookRooms(roomIds);
                break;

            case 5:
                hotel.ShowAllBookedRooms();
                break;

            case 6:
                Console.WriteLine("Enter booking's ID: ");
                int bookingId;
                int.TryParse(Console.ReadLine(), out bookingId);
                
                var invoice = hotel.GenerateInvoice(bookingId);
                Console.WriteLine($"{invoice.Id} - {invoice.TotalPrice}");
                break;

            case 7:
                break;

            default:
                break;
        }

    } catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (option != 7);