﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyKhachSan
{
    public class Booking
    {
        private int _bookingId;
        private DateTime _bookingDate;
        private DateTime _receivingDate;
        private List<Room> _bookingRooms;
        private string _specialRequirement;
        private static int _countId = 1;

        public int BookingId {  get { return _bookingId; } }
        public DateTime BookingDate { get { return _bookingDate; } }
        public DateTime ReceivingDate { get { return _receivingDate; } }
        public  List<Room> BookingRooms { get { return _bookingRooms;} }
        public string SpecialRequirement { get { return _specialRequirement;} }

        public Booking(DateTime bookingDate, DateTime receivingDate, 
            List<Room> bookingRooms, string specialRequirement)
        {
            _bookingId = _countId++;
            _bookingDate = bookingDate;
            _receivingDate = receivingDate;
            _bookingRooms = bookingRooms;
            _specialRequirement = specialRequirement;
        }
    }
}
