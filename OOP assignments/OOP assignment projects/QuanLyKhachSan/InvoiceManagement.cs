﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyKhachSan
{
    public class InvoiceManagement
    {
        private List<Invoice> _invoices;

        public InvoiceManagement() 
        { 
            _invoices = new List<Invoice>();
        }

        public void AddInvoice(Invoice invoice)
        {
            try
            {
                if (_invoices.Contains(invoice))
                {
                    throw new Exception("This invoice has already been existed");
                }
                _invoices.Add(invoice);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Invoice GetInvoice(int id)
        {
            try
            {
                var invoice = _invoices.FirstOrDefault(x => x.BookingDetail.BookingId == id);
                if (invoice is null)
                {
                    throw new Exception("This invoice is not existed");
                }
                return invoice;
            }
            catch(Exception)
            {
                throw;
            }
        }
    }
}
