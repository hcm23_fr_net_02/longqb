﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyKhachSan
{
    public class Hotel
    {
        private List<Room> _roomList;
        private List<Booking> _bookings;
        private InvoiceManagement _invoiceManagement;
        
        public Hotel() 
        { 
            _roomList = new List<Room>();
            _bookings = new List<Booking>();
            _invoiceManagement = new InvoiceManagement();
        }

        public void BookRooms(List<int> ids)
        {
            try
            {
                List<Room> roomList = new List<Room>();

                foreach(var id in ids)
                {
                    var room = FindRoomById(id);
                    if (room.IsBook)
                    {
                        throw new Exception("This room has already been booked. " +
                            "Please choose another room");
                    }
                    room.IsBook = true;
                    roomList.Add(room);
                }

                var newBooking = new Booking(DateTime.Now, DateTime.Now,
                    roomList, string.Empty);
                _bookings.Add(newBooking);
                _invoiceManagement.AddInvoice(new Invoice(
                        new List<IService>(), newBooking));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Room FindRoomById(int id)
        {
            try
            {
                var room = _roomList.FirstOrDefault(r => r.Id == id);

                if (room is null)
                {
                    throw new Exception("The room with this id is not exists.");
                }

                return room;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void AddNewRoom(string roomType, double price, bool isBooked, 
            string conveniences)
        {
            try
            {
                _roomList.Add(new Room(roomType, price, isBooked, conveniences));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void RemoveRoom(int id)
        {
            try
            {
                _roomList.Remove(FindRoomById(id));
            } 
            catch (Exception)
            {
                throw;
            }
        }

        public void ShowAllRoomDetails()
        {
            foreach(var room in _roomList)
            {
                Console.WriteLine($"{room.Id} - {room.Type} - {room.PricePerNight} - " +
                    $"{room.IsBook} - {room.Conveniences}");
            }
        }

        public void ShowAllBookedRooms()
        {
            foreach (var room in _roomList.Where(x => x.IsBook == true))
            {
                Console.WriteLine($"{room.Id} - {room.Type} - {room.PricePerNight} - " +
                    $"{room.IsBook} - {room.Conveniences}");
            }
        }

        public Invoice GenerateInvoice(int bookingId)
        {
            try
            {
                var booking = _bookings.Where(x => x.BookingId == bookingId).FirstOrDefault();

                if (booking is null)
                {
                    throw new Exception("There is no booking with this ID");
                }

                return _invoiceManagement.GetInvoice(bookingId);
            }
            catch (Exception)
            {
                throw;
            }
            
        }
    }
}
