﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyThuVien
{
    public class BookHistory
    {
        private string _bookAuthor;
        private string _bookTitle;
        private int _bookId;
        private DateTime _date;
        private string _status;

        public string BookAuthor { get { return _bookAuthor; } }
        public string BookTitle { get { return _bookTitle; } }
        public int BookId { get { return _bookId;} }
        public DateTime Date { get { return _date;} }
        public string Status { get { return _status;} } 

        public BookHistory(string bookAuthor, string bookTitle, int bookId, 
            DateTime date, string status)
        {
            _bookAuthor = bookAuthor;
            _bookTitle = bookTitle;
            _bookId = bookId;
            _date = date;
            _status = status;
        }
    }
}
