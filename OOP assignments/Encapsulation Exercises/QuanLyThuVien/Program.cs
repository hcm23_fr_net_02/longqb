﻿using QuanLyThuVien;

int option = 1;
Library library = new Library();

while (option != 4)
{
    Console.WriteLine("--------- LIBRARY MANAGEMENT MENU ---------");
    Console.WriteLine("1. Get all the books' information in the library");
    Console.WriteLine("2. Borrow a book");
    Console.WriteLine("3. Return book to the library");
    Console.WriteLine("4. Exit");
    Console.Write("Choose option you want to use: ");
    try
    {
        option = int.Parse(Console.ReadLine());
        
        switch(option)
        {
            case 1:
                library.PrintBookInfo();
                break;
            case 2:
                int bookID;
                int.TryParse(Console.ReadLine(), out bookID);
                library.CheckOut(bookID);
                break;
            case 3:
                int.TryParse(Console.ReadLine(), out bookID);
                library.ReturnBook(bookID);
                break;
            case 4:
                break;
            default:
                break;
        }
    }
    catch(Exception ex)
    {
        Console.WriteLine(ex.ToString());
    }
}