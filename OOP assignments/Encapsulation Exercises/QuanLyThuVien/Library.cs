﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace QuanLyThuVien
{
    public class Library
    {
        private List<Book> _listOfBooks;

        public Library()
        { 
            string data = File.ReadAllText("ListOfBooks.json");
            _listOfBooks = JsonSerializer.Deserialize<List<Book>>(data);
        }

        public void CheckOut(int id)
        {
            var checkOutBook = _listOfBooks.FirstOrDefault(x => x.Id == id);
            if (checkOutBook is not null)
            {
                if (checkOutBook.IsAvailable)
                {
                    checkOutBook.SetAvailable(false);
                    UpdateHistoryFile(checkOutBook, "Borrow");
                    Console.WriteLine("Successfully borrow this book");
                }
                else
                {
                    Console.WriteLine("This book has already been borrowed. " +
                    "Please select another book");
                }
            }
            else
            {
                Console.WriteLine("This book is not exist in the library database");
            }
        }

        public void ReturnBook(int id)
        {
            var book = _listOfBooks.FirstOrDefault(x => x.Id == id);
            if (book is not null)
            {
                book.SetAvailable(true);
                UpdateHistoryFile(book, "Return");
                Console.WriteLine("Successfully return the book to the library");
            }
            else
            {
                Console.WriteLine("This book is not exist in the library database");
            }
        }

        public void PrintBookInfo()
        {
            foreach(var book in _listOfBooks)
            {
                Console.WriteLine($"{book.Id} - {book.Title} - " +
                    $"{book.Author} - {book.IsAvailable}");
            }
        }

        public void UpdateHistoryFile(Book book, string status)
        {
            var history = new BookHistory(book.Author, book.Title, 
                book.Id, DateTime.Now, status);

            var convertedHistoryJson = JsonSerializer.Serialize(history);
            File.AppendAllText(Directory.GetCurrentDirectory() + "\\History.json", 
                convertedHistoryJson);
        }
    }
}
