﻿namespace QuanLyThuVien
{
    public class Book
    {
        private int _id;
        private string _title;
        private string _author;
        private bool _isAvailable;

        public int Id { get { return _id; } }   
        public string Title { get { return _title;} }
        public string Author { get { return _author;} }
        public bool IsAvailable { get { return _isAvailable; } }

        public Book(int id, string title, string author, bool isAvailable)
        {
            _author = author;
            _id = id;
            _title = title;
            _isAvailable = isAvailable;
        }

        public bool SetAvailable(bool available)
        {
            if (_isAvailable == available)
            {
                return false;
            }
            _isAvailable = available;
            return true;
        }
    }
}
