﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncapsulationExercises
{
    public class Customer
    {
        private string _name;
        private string _email;
        private double _accountBalance;

        public string Name { get { return _name; } }
        public string Email { get { return _email; } } 
        public double AccountBalance { get { return _accountBalance;} }

        public Customer(string name, string email)
        {
            _name = name;
            _email = email;
            _accountBalance = 0;
        }

        public bool AddFunds(double amount)
        {
            if (amount >= 0)
            {
                _accountBalance += amount;
                return true;
            }
            return false;
        }

        public bool MakePurchase(double amount)
        {
            if (CanAfford(amount))
            {
                _accountBalance -= amount;
                return true;
            }
            return false;
        }

        public bool CanAfford(double amount)
        {
            if (amount > _accountBalance)
            {
                return false;
            }
            return true;
        }
    }
}
