﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceExercises
{
    public class Cat : Animal
    {
        private string _furColor;
        public string FurColor { get => _furColor; }
        public Cat(string name, string type, int age, string furColor) : base(name, type, age)
        {
            _furColor = furColor;
        }

        public void Meow()
        {
            Console.WriteLine($"Meow! Meow!");
        }
    }
}
