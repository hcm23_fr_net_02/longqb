﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceExercises
{
    public class Dog : Animal
    {
        private string _breed;
        public string Breed { get => _breed; }
        public Dog(string name, string type, int age, string breed) : base(name, type, age)
        {
            _breed = breed;
        }
        public void Bark()
        {
            Console.WriteLine("Woof! Woof!");
        }
    }
}
