﻿using InheritanceExercises;

Dog dog1 = new Dog("Beth", "Corgi", 1, "Corgi");
Cat cat1 = new Cat("Anne", "No idea", 1, "White");

dog1.Bark();
cat1.Meow();

List<Animal> animals = new List<Animal>()
{
    new Dog("Gen", "Husky", 2, "ala"),
    new Cat("Senna", "asc", 1, "Black"),
    new Cat("Falcon", "aasdd", 3, "Yellow"),
    new Dog("Vin", "Deutsch", 1, "Black"),
    new Dog("Kennegh", "Bergie", 3, "Brown"),
    new Cat("Func", "Adra", 1, "Gray")
};

foreach (var animal in animals)
{
    animal.Speak();
}

foreach (var animal in animals)
{
    if (animal.GetType() == typeof(Cat))
    {
        ((Cat)animal).Meow();
    }
    else
    {
        ((Dog)animal).Bark();
    }
}