﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceExercises
{
    public class Animal
    {
        private string _name;
        private string _type;
        private int _age;

        public string Name { get { return _name; } }    
        public int Age { get { return _age;} }
        public string Type { get { return _type;} }

        public Animal(string name, string type, int age)
        {
            _name = name;
            _type = type;
            _age = age;
        }

        public virtual void Speak()
        {
            Console.WriteLine($"The {_type} named {_name} sounds [sound]");
        }
    }
}
