﻿using QuanLySinhVien;

class Program
{
    static void Main()
    {
        StudentManager studentManager = new StudentManager();
        int option = 0;
        do
        {
            Console.WriteLine("--------- STUDENT MANAGER MENU ----------");
            Console.WriteLine("1. Add new student");
            Console.WriteLine("2. Remove a student");
            Console.WriteLine("3. Display all students' information");
            Console.WriteLine("4. Find a student by name");
            Console.WriteLine("5. Exit");
            Console.Write("Choose: ");
            option = int.Parse(Console.ReadLine());
            Console.WriteLine("------------------------------------------");

            switch(option)
            {
                case 1:
                    Console.Write("Enter student's name: ");
                    string name = Console.ReadLine();
                    Console.Write("Enter student's age: ");
                    int age = int.Parse(Console.ReadLine());
                    Console.Write("Enter student's gpa: ");
                    double gpa = double.Parse(Console.ReadLine());
                    Student newStudent = new Student(name, age, gpa);
                    studentManager.AddStudent(newStudent);
                    break;
                case 2:
                    Console.Write("Enter student's name: ");
                    name = Console.ReadLine();
                    studentManager.RemoveStudent(name);
                    break;
                case 3:
                    studentManager.DisplayStudents();
                    break;
                case 4:
                    Console.Write("Enter student's name: ");
                    name = Console.ReadLine();
                    Student student = studentManager.FindStudentByName(name);
                    Console.WriteLine("Student {0}, age {1}, has gpa {2}",
                        student.Name, student.Age, student.Gpa);
                    break;
                default:
                    option = 0;
                    break;
            }
        } while(option != 0);
    }
}