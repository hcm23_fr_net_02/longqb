﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLySinhVien
{
    public class Student
    {
        private string _name;
        private int _age;
        private double _gpa;
        public string Name { get { return _name; } }
        public int Age { get { return _age; } }
        public double Gpa { get { return _gpa; } }

        public Student(string name, int age, double gpa)
        {
            this._age = age;
            this._gpa = gpa;    
            this._name = name;
        }
    }
}
