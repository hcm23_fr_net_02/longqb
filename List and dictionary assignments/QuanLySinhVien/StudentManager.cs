﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLySinhVien
{
    public class StudentManager
    {
        private List<Student> _students;
        public StudentManager()
        {
            _students = new List<Student>();
        }

        public void AddStudent(Student student)
        {
            _students.Add(student);
        }

        public void RemoveStudent(string studentName)
        {
            var student = FindStudentByName(studentName);
            if (student != null)
            {
                _students.Remove(student);
            }
        }

        public void DisplayStudents()
        {
            foreach(var item in _students)
            {
                Console.WriteLine("Student {0}, age {1}, has gpa {2}", 
                    item.Name, item.Age, item.Gpa);
            }
        }

        public Student FindStudentByName(string studentName)
        {
            foreach (var item in _students)
            {
                if (item.Name == studentName)
                {
                    return item;
                }
            }
            return null;
        }
    }
}
