﻿using QuanLyTuDienAnhViet;

class Progarm
{
    static void Main()
    {
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        EnVnDictionary enVn = new EnVnDictionary();

        int n = 1;
        do
        {
            Console.WriteLine("---------- English - Vietnamese Dictionary -----------");
            Console.WriteLine("1. Add new word");
            Console.WriteLine("2. Find a word");
            Console.WriteLine("3. Remove a word");
            Console.WriteLine("4. Display all words");
            Console.WriteLine("5. Exit");
            Console.Write("Choose: ");
            n = int.Parse(Console.ReadLine());

            switch(n)
            {
                case 1:
                    Console.Write("Enter English word: ");
                    string engWord = Console.ReadLine();
                    Console.Write("Enter Vietnamese meaning: ");
                    string vnMeaning = Console.ReadLine();
                    enVn.Add(engWord, vnMeaning);
                    break;
                case 2:
                    Console.Write("Enter the word you want to find Vietnamese meaning: ");
                    string word = Console.ReadLine();
                    Console.WriteLine(enVn.Find(word));
                    break;
                case 3:
                    Console.Write("Enter the word you want to delete: ");
                    word = Console.ReadLine();
                    enVn.Remove(word);
                    break;
                case 4:
                    enVn.Display();
                    break;
                default:
                    n = 0;
                    break;
            }

        }while (n != 0);
    }
}