﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyTuDienAnhViet
{
    public class EnVnDictionary
    {
        Dictionary<string, string> myDict;

        public EnVnDictionary()
        {
            myDict = new Dictionary<string, string>();
        }

        public void Add(string key, string value)
        {
            if (myDict.ContainsKey(key))
            {
                return;
            }
            myDict.Add(key, value);
        }

        public string Find(string key)
        {
            return myDict.ContainsKey(key) ? myDict[key]: "This word is not in the dictionary";
        }

        public void Remove(string value)
        {
            foreach (string key in myDict.Keys)
            {
                if (key.Equals(value) || myDict[key].Equals(value))
                {
                    myDict.Remove(key);
                }
            }
        }

        public void Display()
        {
            foreach(string key in myDict.Keys)
            {
                Console.WriteLine("{0} - {1}", key, myDict[key]);
            }
        }
    }
}
