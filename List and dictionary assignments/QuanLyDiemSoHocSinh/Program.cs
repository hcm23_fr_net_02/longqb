﻿using QuanLyDiemSoHocSinh;

class Program
{
    static void Main()
    {
        StudentManager manager = new StudentManager();
        int option = 1;

        do
        {
            Console.WriteLine("--------- STUDENT MANAGER MENU ----------");
            Console.WriteLine("1. Add new student");
            Console.WriteLine("2. Find gpa of a student");
            Console.WriteLine("3. Display all students' information");
            Console.WriteLine("4. Exit");
            Console.Write("Choose: ");
            option = int.Parse(Console.ReadLine());

            switch(option)
            {
                case 1:
                    Console.Write("Enter student's name: ");
                    string name = Console.ReadLine();
                    Console.Write("Enter student's gpa: ");
                    double gpa = double.Parse(Console.ReadLine());

                    manager.AddNewStudent(name, gpa);
                    break;
                case 2:
                    Console.Write("Enter student's name: ");
                    name = Console.ReadLine();
                    bool res = manager.FindStudent(name, out gpa);
                    if (res)
                    {
                        Console.WriteLine("Student {0} has GPA: {1}", name, gpa);
                    }
                    else
                    {
                        Console.WriteLine("Student is not in the database");
                    }
                    break;
                case 3:
                    manager.Display();
                    break;
                default:
                    option = 0;
                    break;
            }
            
        } while (option != 0);
    }
}