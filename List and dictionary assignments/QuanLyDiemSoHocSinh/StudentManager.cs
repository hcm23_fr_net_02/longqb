﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyDiemSoHocSinh
{
    public class StudentManager
    {
        private Dictionary<string, double> myDict;

        public StudentManager() 
        {
            myDict = new Dictionary<string, double>();
        }

        public void AddNewStudent(string studentName, double gpa)
        {
            if (!myDict.ContainsKey(studentName))
            {
                myDict.Add(studentName, gpa);
            }
        }

        public bool FindStudent(string studentName, out double gpa)
        {
            return myDict.TryGetValue(studentName, out gpa);
        }

        public void Display()
        {
            foreach(var key in myDict.Keys)
            {
                Console.WriteLine("{0}: {1}", key, myDict[key]);
            }
        }
    }
}
