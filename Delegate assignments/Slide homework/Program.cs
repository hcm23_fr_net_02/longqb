﻿T PerformOperation<T>(T param1, T param2, Func<T, T, T> operation)
{
    return operation(param1, param2);
}

string Addition(string a, string b) => a + b;
int Subtraction(int a, int b) => a - b;
double Multiplication(double a, double b) => a * b;
float Division(float a, float b) => a / b;
Fraction Multiply(Fraction a, Fraction b) => Fraction.Multiply(a, b);

Func<string, string, string> Add = Addition;
Console.WriteLine(PerformOperation<string>("abc", "def", Add));

Func<int, int, int> Sub = Subtraction;
Console.WriteLine(PerformOperation<int>(10, 7, Sub));

Func<double, double, double> Mul = Multiplication;
Console.WriteLine(PerformOperation<double>(7, 2.2, Mul));

Func<float, float, float> Divide = Division;
Console.WriteLine(PerformOperation<float>(9, 2.2f, Divide));

Func<Fraction, Fraction, Fraction> MultiplyFraction = Multiply;
Fraction f1 = new Fraction(1, 2);
Fraction f2 = new Fraction(1, 3);
Fraction res = PerformOperation<Fraction>(f1, f2, MultiplyFraction);
Console.WriteLine("{0}/{1}", res.enumarator, res.denominator);

class Fraction
{
    public int enumarator;
    public int denominator;

    public Fraction(int e, int d)
    {
        enumarator = e;
        denominator = d;
    }

    public static Fraction Multiply(Fraction a, Fraction b)
    {
        return new Fraction(a.enumarator * b.enumarator, a.denominator * b.denominator);
    }
}





