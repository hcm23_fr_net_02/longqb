﻿class Program
{
    // MathOperation assignments
    delegate int MathOperation(int a, int b);

    public int Addition(int a, int b) => a + b;
    public int Subtraction(int a, int b) => a - b;
    public int Multiplication(int a, int b) => a * b;
    public int Division(int a, int b) => a / b;

    int PerformOperation(int x, int y, MathOperation operation)
    {
        return operation(x, y);
    }
    // 

    // Custom delegate assignment
    delegate int Custom(int number);

    int Double(int number) => 2 * number;
    int Triple(int number) => number * 3;
    int Square(int number) => number * number;
    int Cube(int number) => number * number * number;
    void PrintResult(int number, Custom operation)
    {
        Console.WriteLine(operation(number));
    }
    //
    static void Main()
    {
        Program p = new Program();

        Console.WriteLine("Math operation assignment");
        MathOperation A = new MathOperation(p.Addition);
        Console.WriteLine("Enter x: ");
        int x = int.Parse(Console.ReadLine());
        Console.WriteLine("Enter y: ");
        int y = int.Parse(Console.ReadLine());
        Console.WriteLine(p.PerformOperation(x, y, A));
        Console.WriteLine();

        Console.WriteLine("Custom delegate assignment");
        Custom D = new Custom(p.Double);
        Custom T = new Custom(p.Triple);
        Custom S = new Custom(p.Square);
        Custom C = new Custom(p.Cube);
        Console.WriteLine("Enter x: ");
        x = int.Parse(Console.ReadLine());
        p.PrintResult(x, D);
        p.PrintResult(x, T);
        p.PrintResult(x, S);
        p.PrintResult(x, C);
    }

}